import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  email;

  public err; 
  user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
    this.user = this.afAuth.authState;
   }
   
   signup(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(res => 
        {
          console.log('Succesful sign up',res);
          this.router.navigate(['/welcome']);
        }
      );
  }

  //logout function
  Logout(){
    this.afAuth.auth.signOut();  
  }

  //login function
  login(email:string, password:string){
    this.afAuth
    .auth.signInWithEmailAndPassword(email,password).then
    (res => { console.log('Succesful login', res);
     this.router.navigate(['/welcome']);
    }
     )
    .catch(function(error) {
      // Handle Errors here.
      // var errorCode = error.code;
      //if not succesful
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
      this.router.navigate(['/login']);
    });
    // .then(
      // res => { console.log('Succesful login', res);
  //  }

  }

}
