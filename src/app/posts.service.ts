import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})

export class PostsService {
  apipost = "https://jsonplaceholder.typicode.com/posts"
  apicomment = "http://jsonplaceholder.typicode.com/comments"

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  allPosts:AngularFirestoreCollection;

  getpostofuser(userId):Observable<any[]>{
        this.allPosts = this.db.collection(`users/${userId}/posts`);
            console.log('Books collection created');
            return this.allPosts.snapshotChanges().pipe(
              map(actions => actions.map(a => {
                const data = a.payload.doc.data();
                data.id = a.payload.doc.id;
                return { ...data };
              }))
            ); 
      }

      //user adding a post
  addpost(userId:string,title:string, body:string){
    const post = {title:title, body:body};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  getPost(){
        return this.http.get<Post[]>(this.apipost);
      }

  getcomment(){
        return this.http.get<Comment[]>(this.apicomment);
      }

  constructor(private http: HttpClient,private db:AngularFirestore) { }
}
