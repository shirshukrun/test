import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {

  constructor(private classervice:ClassifyService, private auth:AuthService) { }

  collectionArry$:Observable<any[]>;
  userId:string;
  
   ngOnInit() {
  //   this.auth.user.subscribe(
  //     user => {
  //       this.userId = user.uid; 
  //       this.collectionArry$ = this.classervice.getDocu(this.userId);
  //   });
   }

}
