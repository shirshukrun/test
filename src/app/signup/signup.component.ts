import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email: string;
  password: string;

  constructor(public authservice:AuthService,
    public router:Router) { }

  onSubmit(){
    this.authservice.signup(this.email, this.password);
    this.router.navigate(['/signup']);
  }


  ngOnInit() {
  }

}
