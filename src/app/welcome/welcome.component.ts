import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  
  constructor(public auth:AuthService) {}

  //creat the properties
  userId:string;
  useremail:string;


  ngOnInit() {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.useremail = user.email;
       }
    )
  }

}
