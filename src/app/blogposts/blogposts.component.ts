import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';//importing the service 
import { AuthService } from '../auth.service';
import { Post } from '../interfaces/post';//importing the interface
import { Comment } from '../interfaces/comment';

//import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-blogposts',
  templateUrl: './blogposts.component.html',
  styleUrls: ['./blogposts.component.css']
})

export class BlogpostsComponent implements OnInit {

  Posts$: Post[];
  userId:string;
  text:string;
  comments$: Comment[];
  postid:number;
  
  constructor(private postsrv: PostsService,private auth:AuthService) { }

  add(title:string,body:string,id:number){
    this.postsrv.addpost(this.userId,title,body)
    this.postid = id;
    this.text = "the post has been saved succesfuly at 'saved posts'"//after clicking save this message will apear
  }
    

  ngOnInit() {
    this.postsrv.getPost()
    .subscribe(data =>this.Posts$ = data );
    this.postsrv.getcomment()
    .subscribe(data =>this.comments$ = data );

    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }

}