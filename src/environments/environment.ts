// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBoqi91qlwrlOYzOhbBCbq3LwjfbPhiAuU",
    authDomain: "test-a6847.firebaseapp.com",
    databaseURL: "https://test-a6847.firebaseio.com",
    projectId: "test-a6847",
    storageBucket: "test-a6847.appspot.com",
    messagingSenderId: "670573104607",
    appId: "1:670573104607:web:7774d76ca2a0e836a9c86a",
    measurementId: "G-QGD9FSVFCD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
